from django.db import models

# Register your models here.
class CarreraANCC(models.Model):
    idCarreraANCC = models.AutoField(primary_key=True)
    nombreCarreraANCC = models.CharField(max_length=100)
    directorCarreraANCC = models.CharField(max_length=100)
    logoCarreraANCC = models.ImageField(upload_to='carrera_logos')
    perfilEgresoCarrera = models.TextField()
    duracionCarrera = models.IntegerField()

    def tieneRelacion(self):
        # Verificar si hay cursos asociados a esta carrera
        return self.cursoancc_set.exists()

    def __str__(self):
        return self.nombreCarreraANCC

class CursoANCC(models.Model):
    idCursoANCC = models.AutoField(primary_key=True)
    nivelCursoANCC = models.CharField(max_length=50)
    descripcionCursoANCC = models.TextField()
    aulaCursoANCC = models.CharField(max_length=50)
    modalidadCursoANCC = models.CharField(max_length=50)
    promedioCursoANCC = models.IntegerField()
    carreraANCC = models.ForeignKey(CarreraANCC, on_delete=models.PROTECT, null=True, blank=True)


    def tieneRelacion(self):
            # Verificar si hay cursos asociados a esta carrera
        return self.asignaturaancc_set.exists()

    def __str__(self):
        return self.descripcionCursoANCC


class AsignaturaANCC(models.Model):
    idAsignaturaANCC = models.AutoField(primary_key=True)
    nombreAsignaturaANCC = models.CharField(max_length=100)
    creditoAsignaturaANCC = models.IntegerField()
    fechaInicioAsignaturaANCC = models.DateField()
    fechaFinalizacionAsignaturaANCC = models.DateField()
    profesorAsignaturaANCC = models.CharField(max_length=100)
    silaboAsignaturaANCC = models.FileField(upload_to='silabos')
    horarioAsignaturaANCC = models.CharField(max_length=100)
    horasTotalAsignaturaANCC = models.IntegerField()
    cursoANCC = models.ForeignKey(CursoANCC, on_delete=models.PROTECT, null=True, blank=True)


    def __str__(self):
        return self.nombreAsignaturaANCC

from django.apps import AppConfig


class MatriculasanccConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.MatriculasANCC'

from django.shortcuts import render, redirect
from django.contrib import messages
from .models import CarreraANCC, CursoANCC, AsignaturaANCC
from django.utils.datastructures import MultiValueDictKeyError
from django.db import transaction
from django.http import HttpResponse
from django.core.mail import send_mail
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.files.storage import default_storage



def plantilla(request):
    return render(request,'plantilla.html')

def listadoCarrerasANCC(request):
    carrera = CarreraANCC.objects.all()
    return render(request, 'listadoCarrerasANCC.html', {'carreraANCC': carrera})

def guardarCarreraANCC(request):
    if request.method == 'POST':
        nombreCarreraANCC = request.POST.get("nombreCarreraANCC")
        directorCarreraANCC = request.POST.get("directorCarreraANCC")
        perfilEgresoCarrera = request.POST.get("perfilEgresoCarrera")
        duracionCarrera = request.POST.get("duracionCarrera")
        logoCarreraANCC = request.FILES.get("logoCarreraANCC")

        try:
            carrera = CarreraANCC.objects.create(
                nombreCarreraANCC=nombreCarreraANCC,
                directorCarreraANCC=directorCarreraANCC,
                perfilEgresoCarrera=perfilEgresoCarrera,
                duracionCarrera=duracionCarrera,
                logoCarreraANCC=logoCarreraANCC
            )
            messages.success(request, 'Carrera guardada exitosamente')
        except Exception as e:
            messages.error(request, f'Error al guardar la carrera: {e}')

    return redirect('/listadoCarrerasANCC/')



def eliminarCarreraANCC(request, idCarreraANCC):
    carreraEliminar = CarreraANCC.objects.get(idCarreraANCC=idCarreraANCC)

    if carreraEliminar.tieneRelacion():
        messages.error(request, 'No se puede eliminar la carrera seleccionado porque está relacionado.')
    else:
        carreraEliminar.delete()
        messages.success(request, 'CARRERA ELIMINADO EXITOSAMENTE')
    return redirect('/listadoCarrerasANCC/')

def editarCarreraANCC(request, idCarreraANCC):
    try:
        carreraEditar = CarreraANCC.objects.get(idCarreraANCC=idCarreraANCC)
        carrera = CarreraANCC.objects.all()
        return render(request, 'editarCarreraANCC.html', {'carrera': carreraEditar, 'carreraANCC': carrera})
    except CarreraANCC.DoesNotExist:
        messages.error(request, 'El género no existe')
        return redirect('/listadoCarrerasANCC/')

def actualizarCarreraANCC(request):
    if request.method == 'POST':
        idCarreraANCC = request.POST.get("idCarreraANCC")
        nombreCarreraANCC = request.POST.get("nombreCarreraANCC")
        directorCarreraANCC = request.POST.get("directorCarreraANCC")
        perfilEgresoCarrera = request.POST.get("perfilEgresoCarrera")
        duracionCarrera = request.POST.get("duracionCarrera")
        logoCarreraANCC = request.FILES.get('logoCarreraANCC')

        try:
            carreraEditar = CarreraANCC.objects.get(idCarreraANCC=idCarreraANCC)

            if logoCarreraANCC:
                carreraEditar.logoCarreraANCC = logoCarreraANCC

            carreraEditar.nombreCarreraANCC = nombreCarreraANCC
            carreraEditar.directorCarreraANCC = directorCarreraANCC
            carreraEditar.perfilEgresoCarrera = perfilEgresoCarrera
            carreraEditar.duracionCarrera = duracionCarrera
            carreraEditar.save()

            messages.success(request, 'Carrera actualizada exitosamente')
        except CarreraANCC.DoesNotExist:
            messages.error(request, 'La carrera no existe')

    return redirect('/listadoCarrerasANCC/')


def listadoCursosANCC(request):
    curso = CursoANCC.objects.all()
    carrera = CarreraANCC.objects.all()
    return render(request, 'listadoCursosANCC.html', {'cursoANCC': curso, 'carreraANCC': carrera})


def eliminarCursoANCC(request, idCursoANCC):
    cursoEliminar = CursoANCC.objects.get(idCursoANCC=idCursoANCC)

    if cursoEliminar.tieneRelacion():
        messages.error(request, 'No se puede eliminar el curso seleccionado porque está relacionado.')
    else:
        cursoEliminar.delete()
        messages.success(request, 'CURSO ELIMINADO EXITOSAMENTE')
    return redirect('/listadoCursosANCC/')


def guardarCursoANCC(request):
    carreraANCC_id = request.POST.get("carreraANCC")

    # Verifica si el ID de la carrera está presente en la solicitud
    if carreraANCC_id is not None:
        try:
            # Capturando la carrera seleccionada por el usuario
            carreraSeleccionada = CarreraANCC.objects.get(idCarreraANCC=carreraANCC_id)

            nivelCursoANCC = request.POST["nivelCursoANCC"]
            descripcionCursoANCC = request.POST["descripcionCursoANCC"]
            aulaCursoANCC = request.POST["aulaCursoANCC"]
            modalidadCursoANCC = request.POST["modalidadCursoANCC"]
            promedioCursoANCC = request.POST["promedioCursoANCC"]

            fotografia_portada = request.FILES.get("fotografia_portada")

            # Insertando datos mediante el ORM de Django
            curso = CursoANCC.objects.create(
                nivelCursoANCC=nivelCursoANCC,
                descripcionCursoANCC=descripcionCursoANCC,
                aulaCursoANCC=aulaCursoANCC,
                modalidadCursoANCC=modalidadCursoANCC,
                promedioCursoANCC=promedioCursoANCC,
                carreraANCC=carreraSeleccionada,
            )

            messages.success(request, 'CURSO GUARDADO EXITOSAMENTE')
        except CarreraANCC.DoesNotExist:
            messages.error(request, 'La carrera seleccionada no existe')

    return redirect('/listadoCursosANCC/')
def editarCursoANCC(request, idCursoANCC):
    cursoEditar = CursoANCC.objects.get(idCursoANCC=idCursoANCC)
    carrera = CarreraANCC.objects.all()
    return render(request, 'editarCursoANCC.html', {'curso': cursoEditar, 'carreraANCC': carrera})

def actualizarCursoANCC(request):
    # Obtener los datos del formulario
    idCursoANCC = request.POST["idCursoANCC"]
    nivelCursoANCC = request.POST["nivelCursoANCC"]
    descripcionCursoANCC = request.POST["descripcionCursoANCC"]
    aulaCursoANCC = request.POST["aulaCursoANCC"]
    modalidadCursoANCC = request.POST["modalidadCursoANCC"]
    promedioCursoANCC = request.POST["promedioCursoANCC"]
    carreraANCC_id = request.POST["carreraANCC"]

    # Capturando la carrera seleccionada por el usuario
    carreraSeleccionada = CarreraANCC.objects.get(idCarreraANCC=carreraANCC_id)

    # Actualizar el curso con los nuevos datos
    cursoEditar = CursoANCC.objects.get(idCursoANCC=idCursoANCC)
    cursoEditar.nivelCursoANCC = nivelCursoANCC
    cursoEditar.descripcionCursoANCC = descripcionCursoANCC
    cursoEditar.aulaCursoANCC = aulaCursoANCC
    cursoEditar.modalidadCursoANCC = modalidadCursoANCC
    cursoEditar.promedioCursoANCC = promedioCursoANCC
    cursoEditar.carreraANCC = carreraSeleccionada

    cursoEditar.save()

    messages.success(request, 'CURSO ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoCursosANCC/')


def listadoAsignaturasANCC(request):
    asignatura = AsignaturaANCC.objects.all()
    curso = CursoANCC.objects.all()
    return render(request, 'listadoAsignaturasANCC.html', {'asignaturaANCC': asignatura, 'cursoANCC': curso})


def eliminarAsignaturaANCC(request, idAsignaturaANCC):
    asignaturaEliminar = AsignaturaANCC.objects.get(idAsignaturaANCC=idAsignaturaANCC)
    asignaturaEliminar.delete()
    messages.success(request, 'ASIGNATURA ELIMINADO EXITOSAMENTE')
    return redirect('/listadoAsignaturasANCC/')



def editarAsignaturaANCC(request, idAsignaturaANCC):
    asignaturaEditar = AsignaturaANCC.objects.get(idAsignaturaANCC=idAsignaturaANCC)
    curso = CursoANCC.objects.all()
    return render(request, 'editarAsignaturaANCC.html', {'asignatura': asignaturaEditar, 'cursoANCC': curso})


def guardarAsignaturaANCC(request):
    # Obtener los datos del formulario
    nombreAsignaturaANCC = request.POST["nombreAsignaturaANCC"]
    creditoAsignaturaANCC = request.POST["creditoAsignaturaANCC"]
    fechaInicioAsignaturaANCC = request.POST["fechaInicioAsignaturaANCC"]
    fechaFinalizacionAsignaturaANCC = request.POST["fechaFinalizacionAsignaturaANCC"]
    profesorAsignaturaANCC = request.POST["profesorAsignaturaANCC"]
    silaboAsignaturaANCC = request.FILES.get("silaboAsignaturaANCC")
    horarioAsignaturaANCC = request.POST["horarioAsignaturaANCC"]
    horasTotalAsignaturaANCC = request.POST["horasTotalAsignaturaANCC"]
    cursoANCC_id = request.POST["cursoANCC"]

    # Capturando el curso seleccionado por el usuario
    cursoSeleccionado = CursoANCC.objects.get(idCursoANCC=cursoANCC_id)

    # Insertar datos mediante el ORM de Django
    asignatura = AsignaturaANCC.objects.create(
        nombreAsignaturaANCC=nombreAsignaturaANCC,
        creditoAsignaturaANCC=creditoAsignaturaANCC,
        fechaInicioAsignaturaANCC=fechaInicioAsignaturaANCC,
        fechaFinalizacionAsignaturaANCC=fechaFinalizacionAsignaturaANCC,
        profesorAsignaturaANCC=profesorAsignaturaANCC,
        silaboAsignaturaANCC=silaboAsignaturaANCC,
        horarioAsignaturaANCC=horarioAsignaturaANCC,
        horasTotalAsignaturaANCC=horasTotalAsignaturaANCC,
        cursoANCC=cursoSeleccionado,
    )

    messages.success(request, 'ASIGNATURA GUARDADA EXITOSAMENTE')
    return redirect('/listadoAsignaturasANCC/')  # Puedes redirigir a la página que desees

def actualizarAsignaturaANCC(request):
    # Obtener los datos del formulario
    idAsignaturaANCC = request.POST["idAsignaturaANCC"]
    nombreAsignaturaANCC = request.POST["nombreAsignaturaANCC"]
    creditoAsignaturaANCC = request.POST["creditoAsignaturaANCC"]
    fechaInicioAsignaturaANCC = request.POST["fechaInicioAsignaturaANCC"]
    fechaFinalizacionAsignaturaANCC = request.POST["fechaFinalizacionAsignaturaANCC"]
    profesorAsignaturaANCC = request.POST["profesorAsignaturaANCC"]
    silaboAsignaturaANCC = request.FILES.get("silaboAsignaturaANCC")
    horarioAsignaturaANCC = request.POST["horarioAsignaturaANCC"]
    horasTotalAsignaturaANCC = request.POST["horasTotalAsignaturaANCC"]
    cursoANCC_id = request.POST["cursoANCC"]

    # Capturando el curso seleccionado por el usuario
    cursoSeleccionado = CursoANCC.objects.get(idCursoANCC=cursoANCC_id)

    # Actualizar la asignatura con los nuevos datos
    asignaturaEditar = AsignaturaANCC.objects.get(idAsignaturaANCC=idAsignaturaANCC)
    asignaturaEditar.nombreAsignaturaANCC = nombreAsignaturaANCC
    asignaturaEditar.creditoAsignaturaANCC = creditoAsignaturaANCC
    asignaturaEditar.fechaInicioAsignaturaANCC = fechaInicioAsignaturaANCC
    asignaturaEditar.fechaFinalizacionAsignaturaANCC = fechaFinalizacionAsignaturaANCC
    asignaturaEditar.profesorAsignaturaANCC = profesorAsignaturaANCC
    asignaturaEditar.silaboAsignaturaANCC = silaboAsignaturaANCC
    asignaturaEditar.horarioAsignaturaANCC = horarioAsignaturaANCC
    asignaturaEditar.horasTotalAsignaturaANCC = horasTotalAsignaturaANCC
    asignaturaEditar.cursoANCC = cursoSeleccionado

    asignaturaEditar.save()

    messages.success(request, 'ASIGNATURA ACTUALIZADA EXITOSAMENTE')
    return redirect('/listadoAsignaturasANCC/')

def vista1(request):
    return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario])

        messages.success(request, 'Se ha enviado tu correo')
        return HttpResponseRedirect('/')  # Puedes redirigir a una página de éxito

    return render(request, 'enviar_correo.html')

from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.plantilla),
    path('listadoCarrerasANCC/', views.listadoCarrerasANCC),
    path('guardarCarreraANCC/', views.guardarCarreraANCC),
    path('eliminarCarreraANCC/<int:idCarreraANCC>/', views.eliminarCarreraANCC),
    path('editarCarreraANCC/<int:idCarreraANCC>/', views.editarCarreraANCC),
    path('actualizarCarreraANCC/', views.actualizarCarreraANCC),

    path('listadoCursosANCC/', views.listadoCursosANCC),
    path('guardarCursoANCC/', views.guardarCursoANCC),
    path('eliminarCursoANCC/<int:idCursoANCC>/', views.eliminarCursoANCC),
    path('editarCursoANCC/<int:idCursoANCC>/', views.editarCursoANCC),
    path('actualizarCursoANCC/', views.actualizarCursoANCC),

    path('listadoAsignaturasANCC/', views.listadoAsignaturasANCC),
    path('guardarAsignaturaANCC/', views.guardarAsignaturaANCC),
    path('eliminarAsignaturaANCC/<int:idAsignaturaANCC>/', views.eliminarAsignaturaANCC),
    path('editarAsignaturaANCC/<int:idAsignaturaANCC>/', views.editarAsignaturaANCC),
    path('actualizarAsignaturaANCC/', views.actualizarAsignaturaANCC),

    path('vista1/', views.vista1, name='vista1'),  # Use 'name' to set the namespace
    path('enviar_correo/',views.enviar_correo, name='enviar_correo'),
]
